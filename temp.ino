#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>
#define LCD_CS A3    
#define LCD_CD A2    
#define LCD_WR A1   
#define LCD_RD A0    
#define BOXSIZE 60
#define MINPRESSURE 10
#define MAXPRESSURE 1000
#define TS_MINX 150
#define TS_MINY 120
#define TS_MAXX 920
#define TS_MAXY 940
#define LCD_RESET A4
#define room 23
#define YP A1  
#define XM A2  
#define YM 7   
#define XP 6
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

#define	BLACK           0x0000
#define	BLUE            0x001F
#define	RED             0xF800
#define	GREEN           0x07E0
#define CYAN            0x07FF
#define MAGENTA         0xF81F
#define YELLOW          0xFFE0 
#define WHITE           0xFFFF



int desiredTemp = room;
float temp;
float reading;
float voltage;

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, 0);
int COLUMNS[] = {
  3, 4, 5, 6, 7, 8
};
int COLUMNS_LENGTH = sizeof(COLUMNS) / sizeof(int);

int ROWS[] = {
  9, 10, 11, 12
};
int ROWS_LENGTH = sizeof(ROWS) / sizeof(int);
void setup(void) {
  Serial.begin(9600);
  Serial.println("8 Bit LCD test!");
  for (int i = 0; i < COLUMNS_LENGTH; i++) {
    pinMode(COLUMNS[i], OUTPUT);
    digitalWrite(COLUMNS[i], HIGH);
  }
  pinMode(22, OUTPUT);
  for (int i = 0; i < ROWS_LENGTH; i++) {
    pinMode(ROWS[i], OUTPUT);
    digitalWrite(ROWS[i], LOW);
  }

  
  tft.reset();
  
  uint16_t identifier = tft.readRegister(0x0);
  if (identifier == 0x9325) {
    Serial.println("Found ILI9325");
  } else if (identifier == 0x9328) {
    Serial.println("Found ILI9328");
  } else if (identifier == 0x7575) {
    Serial.println("Found HX8347G");
  } else {
    Serial.print("Unknown driver chip ");
    Serial.println(identifier, HEX);
    while (1);
  }  
 
  tft.begin(identifier);
  
  tft.fillScreen(WHITE);
  tft.drawRect((BOXSIZE/2), 180, (BOXSIZE/1.5), (BOXSIZE*1.7), BLACK);
  tft.setRotation(3);
  tft.setCursor(40, 40);
  tft.setTextColor(RED);
  tft.setTextSize(2);
  tft.println("Increase");
  tft.setRotation(0);
  tft.drawRect((BOXSIZE/2), 40, (BOXSIZE/1.5), (BOXSIZE*1.7), BLACK);
  tft.setRotation(3);
  tft.setCursor(181, 40);
  tft.setTextColor(RED);
  tft.setTextSize(2);
  tft.println("Decrease");
  pinMode(13, OUTPUT);
  
}  

unsigned long printMillis;
float prevTemp = temp;
float prevDesiredTemp = desiredTemp;

void loop(){
    delay(10);
    
    digitalWrite(13, HIGH);
    Point p = ts.getPoint();
    delay(10);
    digitalWrite(13, LOW);
    delay(10);
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    
    reading = analogRead(7);
    delay(10);
    voltage = ((reading*4.5 )/1024);
    temp = (voltage-0.5)*100;
    
    if (temp > (desiredTemp-1)){
      digitalWrite(22, LOW);
    
    }
    
    if(temp < (desiredTemp-4)){
      digitalWrite(22, HIGH);
    }
    //if you touch the screen or if the difference
    //between current and previous temperature is more than 1.5
    //refresh the readings
    if ((p.z > MINPRESSURE && p.z < MAXPRESSURE) || (abs(temp-prevTemp) > 1.5) || (prevDesiredTemp != desiredTemp)){
      Serial.println();
      Serial.print("X = "); Serial.print(p.x);
      Serial.print("Y = "); Serial.print(p.y);
      Serial.print("\tPressure = "); Serial.println(p.z);
      //coordinates of INCREASE button
      if ((p.x > 700) && (p.x < 815) && (p.y > 200) && (p.y < 450)){
        desiredTemp += 1;
      }
      //coordinates of DECREASE button
      else if((p.x > 700) && (p.x < 815) && (p.y > 550) && (p.y < 750)){
        desiredTemp -= 1;

      }
      p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
      p.y = map(p.y, TS_MINY, TS_MAXY, tft.height(), 0);
      Serial.print("voltage is: ");
      Serial.println(voltage);
      Serial.print("Desired temp is: ");
      Serial.println(desiredTemp);
      Serial.print("Current temp is: ");
      Serial.println(temp);
      Serial.println(p.x);
      Serial.println(p.y);
      tft.fillRect(210, 70, 90, 110, WHITE);
      tft.setTextColor(RED);
      tft.setTextSize(2);
      tft.setCursor(60, 90);
      tft.print("Desired temp: ");tft.println(desiredTemp);
      tft.setCursor(60, 120);
      tft.print("Current temp: ");tft.println(temp);
      prevTemp = temp;
      prevDesiredTemp = desiredTemp;
    }
    
}

